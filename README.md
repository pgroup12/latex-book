# latex book

Project is work in progress, however it is already usable


## Markdown based template for latex

Requirements:
* Bash
* LuaLaTeX, for example from TeXLive
* LaTeX Libraries:

geometry

inputenc - most probably not needed, as lualatex is unicode

babel
markdown
graphicx
fontspec
unicode-math
hyphenat
fancyhdr
multicol
wrapfig

actually many of these libs aren't needed, will cleanup in next release

## How to use:
1. Place Markdown files in md folder.
They should be auto discovered in next step
2. Run createpdf.sh file (after adapting it to your needs)

tested and running on Linux Mint 19, Android with Termux

## Disclaimer
THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
